package com.shlok.tictactoe;

import android.widget.Button;

public class ButtonClass {
    Button b00,b01, b02, b10, b11, b12, b20, b21, b22;

    public ButtonClass(Button b00, Button b01, Button b02, Button b10, Button b11, Button b12, Button b20, Button b21, Button b22) {
        this.b00 = b00;
        this.b01 = b01;
        this.b02 = b02;
        this.b10 = b10;
        this.b11 = b11;
        this.b12 = b12;
        this.b20 = b20;
        this.b21 = b21;
        this.b22 = b22;
    }

    public Button getB00() {
        return b00;
    }

    public void setB00(Button b00) {
        this.b00 = b00;
    }

    public Button getB01() {
        return b01;
    }

    public void setB01(Button b01) {
        this.b01 = b01;
    }

    public Button getB02() {
        return b02;
    }

    public void setB02(Button b02) {
        this.b02 = b02;
    }

    public Button getB10() {
        return b10;
    }

    public void setB10(Button b10) {
        this.b10 = b10;
    }

    public Button getB11() {
        return b11;
    }

    public void setB11(Button b11) {
        this.b11 = b11;
    }

    public Button getB12() {
        return b12;
    }

    public void setB12(Button b12) {
        this.b12 = b12;
    }

    public Button getB20() {
        return b20;
    }

    public void setB20(Button b20) {
        this.b20 = b20;
    }

    public Button getB21() {
        return b21;
    }

    public void setB21(Button b21) {
        this.b21 = b21;
    }

    public Button getB22() {
        return b22;
    }

    public void setB22(Button b22) {
        this.b22 = b22;
    }
}
