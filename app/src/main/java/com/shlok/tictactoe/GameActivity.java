package com.shlok.tictactoe;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class GameActivity extends AppCompatActivity {

    private Button[][] buttons = new Button[3][3];
    private TextView textViewPlayer1, textViewPlayer2;
    private TextView roomIDText, youAreTextView;

    private Button resetButton;

    private Boolean player1Turn;
    private int chanceCount;
    private int player1Points, player2Points;

    private Boolean isPLayer1;

    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    int roomID;
    private boolean chanceError = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("TicTacToe");

        roomID = getIntent().getIntExtra("Room ID", -1);
        isPLayer1 = getIntent().getBooleanExtra("isPlayer1", false);

        textViewPlayer1 = (TextView) findViewById(R.id.textViewPlayer1);
        textViewPlayer2 = (TextView) findViewById(R.id.textViewPlayer2);
        roomIDText = (TextView) findViewById(R.id.roomIDTextView);
        youAreTextView = (TextView) findViewById(R.id.youAreTextView);

        roomIDText.setText("Room ID: " + roomID);

        if(isPLayer1){
            youAreTextView.setText("You Are: Player 1");
        }
        else{
            youAreTextView.setText("You Are: Player 2");
        }
        player1Turn = true;

        for(int i=0; i<3; i++){
            for(int j=0; j<3; j++){
                final String buttonID = "button" + i + j;
                int resID = getResources().getIdentifier(buttonID, "id", getPackageName());
                buttons[i][j] = findViewById(resID);
                final int finalI = i;
                final int finalJ = j;

                realtimeUpdate();


                buttons[i][j].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(!((Button) view).getText().toString().equals("")){
                            return;
                        }

                        if(player1Turn && isPLayer1){
                            ((Button) view).setText("X");
                            databaseReference.child(""+roomID).child("status").child(""+finalI+finalJ).setValue("X");
                            chanceCount++;
                            chanceError = false;

                        }
                        else if(player1Turn && !isPLayer1){
                            Toast.makeText(GameActivity.this, "Player 1's Turn!!!", Toast.LENGTH_SHORT).show();
                            chanceError = true;

                        }
                        else if(!player1Turn && !isPLayer1){
                            ((Button) view).setText("O");
                            databaseReference.child(""+roomID).child("status").child(""+finalI+finalJ).setValue("0");
                            chanceCount++;
                            chanceError = false;
                        }
                        else if(!player1Turn && isPLayer1){
                            Toast.makeText(GameActivity.this, "Player 2's Turn!!!", Toast.LENGTH_SHORT).show();
                            chanceError = true;

                        }

                        if(checkForWin()){
                            if(player1Turn){
                                player1Wins();
                            }
                            else{
                                player2Wins();
                            }
                        }
                        else if(chanceCount == 9){
                            draw();
                        }
                        else{
                            if(!chanceError)
                            player1Turn = !player1Turn;
                        }
                    }
                });
            }
        }

        resetButton = findViewById(R.id.resetButton);
        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //player1Points = player2Points = 0;
                resetBoard();
                updateScore();
            }
        });

    }

    private void realtimeUpdate() {
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(int i=0; i<3; i++)
                    for(int j=0; j<3; j++)
                        if(dataSnapshot.child(""+roomID).child("status").child(""+i+j).exists()) {
                            if (buttons[i][j].getText().equals("")) {
                                String input = dataSnapshot.child("" + roomID).child("status").child("" + i + j).getValue().toString();
                                buttons[i][j].setText(input);
                                if(input.equals("X"))
                                    player1Turn = false;
                                else
                                    player1Turn = true;
                            }
                        }
                        else{
                            buttons[i][j].setText("");
                        }

                if(!dataSnapshot.child(""+roomID).child("status").hasChildren())
                    player1Turn = true;

                if(dataSnapshot.child(""+roomID).child("Player 1").child("Score").exists()) {
                    textViewPlayer1.setText("Player 1: " + dataSnapshot.child(""+roomID).child("Player 1").child("Score").getValue().toString());
                }
                if(dataSnapshot.child(""+roomID).child("Player 2").child("Score").exists()) {
                    textViewPlayer2.setText("Player 2: " + dataSnapshot.child(""+roomID).child("Player 2").child("Score").getValue().toString());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private boolean checkForWin(){
        String[][] field = new String[3][3];

        for(int i=0; i<3; i++)
            for(int j=0; j<3; j++)
                field[i][j] = buttons[i][j].getText().toString();

        for(int i=0; i<3; i++){
            if(field[i][0].equals(field[i][1]) && field[i][0].equals(field[i][2]) && !field[i][0].equals("")){
                return true;
            }
        }

        for(int i=0; i<3; i++){
            if(field[0][i].equals(field[1][i]) && field[0][i].equals(field[2][i]) && !field[0][i].equals("")){
                return true;
            }
        }

        if(field[0][0].equals(field[1][1]) && field[0][0].equals(field[2][2]) && !field[0][0].equals("")){
            return true;
        }

        if(field[0][2].equals(field[1][1]) && field[0][2].equals(field[2][0]) && !field[0][2].equals("")){
            return true;
        }

        return false;
    }

    private void player1Wins(){
        player1Points++;
        Toast.makeText(this, "Player 1 Wins!", Toast.LENGTH_SHORT).show();
        updateScore();
        resetBoard();
    }

    private void player2Wins(){
        player2Points++;
        Toast.makeText(this, "Player 2 Wins!", Toast.LENGTH_SHORT).show();
        updateScore();
        resetBoard();
    }

    private void draw(){
        Toast.makeText(this, "Draw!", Toast.LENGTH_SHORT).show();
        resetBoard();
    }

    private void updateScore(){
        databaseReference.child(""+roomID).child("Player 1").child("Score").setValue(""+player1Points);
        databaseReference.child(""+roomID).child("Player 2").child("Score").setValue(""+player2Points);
    }

    private void resetBoard(){
        clearOnlineData();
        chanceCount = 0;
        player1Turn = true;
    }

    private void clearOnlineData(){
        databaseReference.child(""+roomID).child("status").removeValue();
    }
}
