package com.shlok.tictactoe;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private Button createGRBtn, joinGRBtn;
    private int roomID = 0;

    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        createGRBtn = (Button) findViewById(R.id.createGRBtn);
        joinGRBtn = (Button) findViewById(R.id.joinGRBtn);

        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("TicTacToe");

        createGRBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                roomID = generateRoomID();
                databaseReference.child(""+roomID).child("").setValue("");
                //databaseReference.child(key).child("Room ID").setValue(roomID);
                Toast.makeText(MainActivity.this, ""+roomID, Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(MainActivity.this, GameActivity.class);
                intent.putExtra("from", "Create Button");
                intent.putExtra("Room ID", roomID);
                intent.putExtra("isPlayer1", true);
                startActivity(intent);
            }
        });

        joinGRBtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(MainActivity.this);
                dialog.setContentView(R.layout.alert_layout);
                dialog.setTitle("Enter Game Room ID: ");
                Button enterBtn = (Button) dialog.findViewById(R.id.enterButton);

                enterBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        EditText editText = (EditText) dialog.findViewById(R.id.roomIDEditText);
                        int roomID = (Integer.valueOf(editText.getText().toString()));
                        if(roomID == -1)
                            Toast.makeText(MainActivity.this, "Please enter a valid Room ID!", Toast.LENGTH_SHORT).show();
                        else{
                            dialog.dismiss();
                            Intent intent = new Intent(MainActivity.this, GameActivity.class);
                            intent.putExtra("from", "Join Button");
                            intent.putExtra("Room ID", roomID);
                            intent.putExtra("isPlayer1", false);

                            startActivity(intent);
                        }
                    }
                });

                dialog.show();
            }
        });
    }

    public int generateRoomID(){
        Random random = new Random();
        return random.nextInt(9999);
    }
}
